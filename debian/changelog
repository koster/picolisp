picolisp (24.9-1) unstable; urgency=medium

  * New upstream version 24.9

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 29 Sep 2024 20:50:26 +0900

picolisp (24.6-1) unstable; urgency=medium

  * New upstream version 24.6

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 30 Jun 2024 18:50:45 +0900

picolisp (24.3-1) unstable; urgency=medium

  * New upstream version 24.3
  * debian/control: build-dep pkg-config -> pkgconf
  * debian/control: bump standards-version to 4.7.0, no changes needed

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 07 Apr 2024 22:22:51 +0900

picolisp (23.12-1) unstable; urgency=medium

  * New upstream version 23.12

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 26 Dec 2023 20:30:42 +0900

picolisp (23.9-1) unstable; urgency=medium

  * New upstream version 23.9

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 02 Oct 2023 08:16:20 +0900

picolisp (23.6-1) unstable; urgency=medium

  * New upstream version 23.6
  * debian/control: remove s390x
  * debian/picolisp.doc-base: fix index page path
  * debian/patches: fix bootstrap llir

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 01 Jul 2023 16:12:19 +0900

picolisp (23.3-2) unstable; urgency=medium

  * New upstream version 23.3 uploaded to unstable

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 21 Jun 2023 07:13:26 +0900

picolisp (23.3-1) experimental; urgency=medium

  [ Kan-Ru Chen ]
  * New upstream version 23.3
  * d/picolisp.install: add bin/pty
  * d/patches: drop shbang patches

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 02 Apr 2023 08:34:42 +0900

picolisp (23.2-1) unstable; urgency=medium

  * New upstream version 23.2
  * debian/control: bump standards-version to 4.6.2, no changes needed

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 08 Feb 2023 21:05:43 +0900

picolisp (22.12-1) unstable; urgency=medium

  * New upstream version 22.12

 -- Kan-Ru Chen <koster@debian.org>  Wed, 28 Dec 2022 10:25:18 +0800

picolisp (22.9-1) unstable; urgency=medium

  * New upstream version 22.9

 -- Kan-Ru Chen <koster@debian.org>  Sat, 01 Oct 2022 21:32:28 +0900

picolisp (22.6-1) unstable; urgency=medium

  [ Kan-Ru Chen ]
  * New upstream version 22.6

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 02 Jul 2022 22:42:40 +0900

picolisp (22.3-1) unstable; urgency=medium

  * New upstream version 22.3
  * Refresh patches

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 21 Mar 2022 17:00:03 +0900

picolisp (21.12-2) unstable; urgency=medium

  * debian/patches: Apply a database bug fix patch from upstream

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 04 Jan 2022 20:03:47 +0900

picolisp (21.12-1) unstable; urgency=medium

  * New upstream version 21.12
  * Update lintian-overrides
  * debian/control: bump Standards-Version to 4.6.0, no changes needed.
  * debian/control: add w3m to suggests as the default doc viewer

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 27 Dec 2021 23:45:21 +0900

picolisp (21.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 15 Aug 2021 23:03:04 +0900

picolisp (21.6-1) experimental; urgency=medium

  * New upstream version 21.6

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 15 Aug 2021 23:02:30 +0900

picolisp (21.1.8-1) unstable; urgency=medium

  * New upstream version 21.1.8
    - Fixes build pil64 with pil21 (Closes: #978336)
  * debian/rules: install bash_completion file
  * debian/NEWS: mention how to use lib/compat.l

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 09 Jan 2021 09:33:36 +0900

picolisp (21.0.0+20201226-3) unstable; urgency=medium

  * debian/picolisp.links: Link usr/lib/picolisp/doc to usr/share/doc/picolisp/doc
  * debian/copyright: Update the binary license of balance, httpGate, and ssl

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 03 Jan 2021 22:10:06 +0900

picolisp (21.0.0+20201226-2) unstable; urgency=medium

  * Move nobootstrap option handling into override_dh_auto_build
  * debian/control: specify supported 64-bit architectures

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 27 Dec 2020 08:32:13 +0900

picolisp (21.0.0+20201226-1) unstable; urgency=medium

  * New upstream version 21.0.0
  * Refresh patches
  * Support nobootstrap build option for building on new architecture
  * debian/50picolisp.el is unshipped

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 26 Dec 2020 18:45:13 +0900

picolisp (20.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in debian/watch.

  [ Kan-Ru Chen ]
  * New upstream version 20.6

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 30 Jun 2020 00:04:08 +0900

picolisp (19.12-1) unstable; urgency=medium

  * New upstream version 19.12
  * debian/control: Bump Standards-Versoin to 4.4.1, no changes required

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 02 Jan 2020 21:07:25 +0900

picolisp (19.6-2) unstable; urgency=medium

  * Install missed file /usr/lib/picolisp/lib/vip/draw.l
  * Switch dh compat to 12

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 08 Jul 2019 00:56:46 +0900

picolisp (19.6-1) unstable; urgency=medium

  * New upstream version 19.6
  * Refresh patches
  * debian/50picolisp.el: Don't override picolisp-mode defined variables
  * debian/50picolisp.el: Add autoload for picolisp-wiki-mode.
    Thanks to Jean-Christophe Helary for the suggestion

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 07 Jul 2019 12:31:26 +0900

picolisp (18.12-1) unstable; urgency=medium

  * New upstream version 18.12
  * debian/rules: Build 64 bit binary with system picolisp
  * debian/control: Bump Standards-Version to 4.3.0. No changes required

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 03 Jan 2019 22:04:15 +0900

picolisp (18.6-1) unstable; urgency=medium

  * New upstream version 18.6
  * Bump Standards-Version to 4.1.4. No changes required

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 02 Jul 2018 08:39:10 +0900

picolisp (17.12+20180218-1) unstable; urgency=medium

  * New upstream version 17.12+20180218
    - Fixed relocation error in ext.l and ht.l. (Closes: 889911)
  * Refresh patches

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 18 Feb 2018 09:19:35 +0900

picolisp (17.12-2) unstable; urgency=medium

  * Fix bash_completion

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 17 Jan 2018 09:34:56 +0900

picolisp (17.12-1) unstable; urgency=medium

  * New upstream version 17.12
    (Closes: #857277)
  * debian/control: Update Vcs-{Git,Browser} to Salsa
  * debian/rules: Don't set DEB_HOST_ARCH_* variables
  * debian/rules: Remove unused get-orig-source target
  * Refresh patches
  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat level to 11
  * Support arm64 build (Closes: #871515)

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 07 Jan 2018 14:14:11 +0900

picolisp (17.6-1) unstable; urgency=medium

  * New upstream version 17.6
  * Refresh patches
  * debian/control: Bump Standards-Version to 4.0.0.
    Updated copyright-format url. No other changes needed.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 12 Jul 2017 07:15:16 +0800

picolisp (16.12-1) unstable; urgency=medium

  * New upstream version 16.12

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 10 Dec 2016 21:31:05 -1000

picolisp (16.11.6-1) unstable; urgency=medium

  * New upstream version 16.11.6
  * debian/compat: Change dh compatibility to 9
  * Require dpkg-dev >= 1.18.11 to build without PIE enabled. (Closes: #837573)

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 09 Nov 2016 09:08:14 +0800

picolisp (16.6-2) unstable; urgency=medium

  * Import upstream changes.
    - Closes: #831397

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sun, 17 Jul 2016 22:59:31 +0800

picolisp (16.6-1) unstable; urgency=medium

  * New upstream release.
  * Update minimum java bytecode version used by mkJar to Java7.
  * Bump Standards-Version to 3.9.8, no changes needed.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 04 Jul 2016 23:32:05 +0800

picolisp (16.2-2) unstable; urgency=medium

  * Fix lib/ext loading problem on pil32.
  * Import upstream minor version update and a fix to pil64.
  * Bump Standards-Version to 3.9.7, no changes needed

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 22 Feb 2016 01:54:15 +0800

picolisp (16.2-1) unstable; urgency=medium

  [ Kan-Ru Chen (陳侃如) ]
  * New upstream release
  * Fix minimum java bytecode version used by mkJar
  * Install correct upstream changelog file
  * Do not generate incorrect dbgsym package

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 20 Feb 2016 17:42:27 +0800

picolisp (15.11-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches
  * debian/patches/0004-picolisp_reproducible_build.patch:
    Make amd64 build reproducible
  * Fix FTBFS on hurd-i386. Thanks Svante Signell for the patch.
    (Closes: 802602)
  * Build x86_64 and ppc64el with ersatz.
    Remove dependency of gcc-multilib.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 16 Dec 2015 00:19:17 +0800

picolisp (3.1.11.1-1) unstable; urgency=medium

  * New upstream release; added a missing file svg.l from previous release

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 25 Jun 2015 12:12:50 -0700

picolisp (3.1.11-1) unstable; urgency=medium

  * New upstream release
  * Install bash completion to usr/share/bash-completion/completions

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 24 Jun 2015 07:47:24 -0700

picolisp (3.1.10-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 21 Apr 2015 00:44:55 +0800

picolisp (3.1.9.7-2) unstable; urgency=medium

  * Sync to upstream tip.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Sat, 07 Feb 2015 18:38:38 +0800

picolisp (3.1.9.7-1) unstable; urgency=medium

  * New upstream release
  * Build *.jar from source
  * Do not install doc/{db,utf8} as requested by upstream

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Fri, 06 Feb 2015 00:19:20 +0800

picolisp (3.1.9.3-1) unstable; urgency=medium

  * New upstream release
  * Upload to unstable

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 20 Jan 2015 00:29:25 +0800

picolisp (3.1.9.2-1) experimental; urgency=medium

  * New upstream release
  * Refresh patches

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Tue, 06 Jan 2015 23:28:48 +0800

picolisp (3.1.8.0-4) unstable; urgency=medium

  * Really fix FTBFS on armhf

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Fri, 03 Oct 2014 02:21:16 +0800

picolisp (3.1.8.0-3) unstable; urgency=medium

  * Force ARM mode to fix FTBFS on armhf (Closes: #763764)

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Fri, 03 Oct 2014 01:22:26 +0800

picolisp (3.1.8.0-2) unstable; urgency=medium

  * Use build directory as HOME when running test, fix FTBFS (Closes: #763663)

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 02 Oct 2014 09:38:34 +0800

picolisp (3.1.8.0-1) unstable; urgency=medium

  * Imported Upstream version 3.1.8.0
  * Refresh patches
  * Fix typo in Suggests: tinymce (Closes: #762507)
  * Run tests after build
  * Fix lintian warning empty-short-license-in-dep5-copyright
  * Bump Standards-Version to 3.9.6, no changes needed

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 01 Oct 2014 23:37:52 +0800

picolisp (3.1.7.3-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Use dpkg-buildflags instead of hardening-includes.
  * Do not use invalid architecture wildcards any-armel and
    any-armhf. (Closes: #749119)

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 17 Jul 2014 23:40:45 +0800

picolisp (3.1.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/control:
    - Suggests tinymice.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 31 Mar 2014 00:06:09 +0800

picolisp (3.1.5.2-2) unstable; urgency=medium

  * Sync to tip.
  * debian/picolisp.install:
    - Include lib/phone.css
    - Install arch-indep files under /usr/share/picolisp
  * debian/source/lintian-overrides:
    - Override unknown architectures armel and armhf.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 15 Jan 2014 22:59:02 +0800

picolisp (3.1.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/control:
    - Bump Standards-Version to 3.9.5.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 15 Jan 2014 00:41:33 +0800

picolisp (3.1.4.9-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * debian/rules:
    - Checkout specific release tag directly.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 31 Oct 2013 18:19:34 +0800

picolisp (3.1.2.12-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * debian/control:
    - Bump Standards-Version to 3.9.4.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 20 Jun 2013 21:52:27 +0800

picolisp (3.1.1.15-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Wed, 06 Mar 2013 00:21:35 +0800

picolisp (3.1.1.1-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright:
    - Update copyright for new files.
  * debian/rules:
    - Include `dpkg-buildflags --get CPPFLAGS'.

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Thu, 06 Dec 2012 10:22:21 +0800

picolisp (3.1.0.7-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.
  * debian/control:
    - Update Vcs-* url

 -- Kan-Ru Chen <koster@debian.org>  Mon, 25 Jun 2012 14:30:32 +0800

picolisp (3.1.0.4-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.

 -- Kan-Ru Chen <koster@debian.org>  Wed, 02 May 2012 09:01:32 +0800

picolisp (3.0.9.7-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.
  * Enable hardening flags on helper binaries. (Closes: #663355)
  * debian/control:
    - Build-Depends on lib32gcc1 for amd64 to workaround #667519

 -- Kan-Ru Chen <koster@debian.org>  Sun, 08 Apr 2012 21:31:01 +0800

picolisp (3.0.9.4-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.
  * debian/patches/picolisp_build_on_multi_cpu.patch:
    - Correctly append CFLAGS to upstream CFLAGS. Fixes FTBFS on armhf.
      Thanks to Peter Green and Hector Oron (Closes: #660324).
  * debian/copyright:
    - Update copyright-format to 1.0.
  * debian/control:
    - Bump Standards-Version to 3.9.3.

 -- Kan-Ru Chen <koster@debian.org>  Mon, 05 Mar 2012 22:41:16 +0800

picolisp (3.0.9.3-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 17 Feb 2012 23:07:13 +0800

picolisp (3.0.9.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Add hardening-includes to build dependency.
  * debian/rules:
    - Enable hardening flags.
    - Install bash_completion.

 -- Kan-Ru Chen <koster@debian.org>  Mon, 02 Jan 2012 11:25:29 +0800

picolisp (3.0.8.10-1) unstable; urgency=low

  * New upstream release.

 -- Kan-Ru Chen <koster@debian.org>  Sat, 17 Dec 2011 10:42:10 +0800

picolisp (3.0.8.7-1) unstable; urgency=low

  * New upstream release.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 13 Nov 2011 17:15:44 +0800

picolisp (3.0.8.5-2) unstable; urgency=low

  * Sync to upstream tip with important fixes.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 28 Oct 2011 00:07:36 +0800

picolisp (3.0.8.5-1) unstable; urgency=low

  * New upstream release.
    - Force linkage with libm, fix 64-bit build in Ubuntu. (LP: #874841)
  * Sync to upstream tip with IPv6 support.

 -- Kan-Ru Chen <koster@debian.org>  Thu, 27 Oct 2011 11:29:38 +0800

picolisp (3.0.8.1-2) unstable; urgency=low

  * Add back accidentally removed file to non-amd64 arch.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 02 Oct 2011 18:47:24 +0800

picolisp (3.0.8.1-1) unstable; urgency=low

  * New upstream release.

 -- Kan-Ru Chen <koster@debian.org>  Sat, 01 Oct 2011 22:42:46 +0800

picolisp (3.0.7.7-2) unstable; urgency=low

  * Sync to upstream tip.
    - Fix FTBFS with -Wl,--as-needed flag. (LP: #831180)

 -- Kan-Ru Chen <koster@debian.org>  Wed, 07 Sep 2011 06:08:28 +0800

picolisp (3.0.7.7-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 04 Sep 2011 14:51:15 +0800

picolisp (3.0.7.6-2) unstable; urgency=low

  * Apply patches from upstream.
    - Fix 64-bit random seed initialization.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 19 Aug 2011 18:40:16 +0800

picolisp (3.0.7.6-1) unstable; urgency=low

  * New upstream release.
    - Fix a bug in reading from a pipe from stdin.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 19 Aug 2011 10:40:41 +0800

picolisp (3.0.7.4-1) unstable; urgency=low

  * New upstream release.
  * Sync to upstream tip.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 24 Jul 2011 09:27:15 +0200

picolisp (3.0.7.2-2) unstable; urgency=low

  * Sync to upstream tip.
  * Drop two unused patches.

 -- Kan-Ru Chen <koster@debian.org>  Mon, 11 Jul 2011 17:48:35 +0800

picolisp (3.0.7.2-1) unstable; urgency=low

  * New upstream release.
  * Fix *OS should be "Linux" on Debian system.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 10 Jul 2011 16:29:34 +0800

picolisp (3.0.7.1-1) unstable; urgency=low

  * New upstream release.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 08 Jul 2011 01:32:42 +0800

picolisp (3.0.6.14-1) unstable; urgency=low

  * New upstream release.
  * Install plmod to usr/lib/picolisp/bin (Closes: #627451)

 -- Kan-Ru Chen <koster@debian.org>  Mon, 30 May 2011 22:33:54 +0800

picolisp (3.0.6.11-1) unstable; urgency=low

  * New upstream release.
  * Don't compress .l files.

 -- Kan-Ru Chen <koster@debian.org>  Fri, 13 May 2011 15:55:13 +0800

picolisp (3.0.6.10-2) unstable; urgency=low

  * Add missing build dependency.

 -- Kan-Ru Chen <koster@debian.org>  Thu, 12 May 2011 04:53:33 +0800

picolisp (3.0.6.10-1) unstable; urgency=low

  * New upstream release.
  * Import new picolisp.el from upstream.
  * Install additional supporting binaries to /usr/lib/picolisp/bin.
  * Stop shipping "simul/" and "rcsim/".
  * Add lintian overrides for OpenSSL linkage.

 -- Kan-Ru Chen <koster@debian.org>  Wed, 11 May 2011 15:44:30 +0800

picolisp (3.0.6.9-1) unstable; urgency=low

  * New upstream release.
  * With consensus of upstream, bundled applications now are installed to
    /usr/share/picolisp.
  * Fix typo in package description, thanks to Vincent Blut.
    (Closes: #624454)
  * Add any-armhf to arch list. (Closes: #623351)

 -- Kan-Ru Chen <koster@debian.org>  Mon, 09 May 2011 17:29:45 +0800

picolisp (3.0.6.5-2) unstable; urgency=low

  * Move bundled applications to /usr/lib/picolisp.
  * Fix shebangs for psh, replica and watchdog.

 -- Kan-Ru Chen <koster@debian.org>  Thu, 28 Apr 2011 14:09:35 +0800

picolisp (3.0.6.5-1) unstable; urgency=low

  * New upstream release.
  * Install img, loc, doc, src and src64 as required by upstream.
  * Fix mips, mipsel and armhf build. (Closes: #623351)
  * Bump Standards-Version to 3.9.2, no changes needed.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 24 Apr 2011 16:20:39 +0800

picolisp (3.0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Fix FTBFS on Armel and S/390.

 -- Kan-Ru Chen <koster@debian.org>  Wed, 13 Apr 2011 22:03:26 +0800

picolisp (3.0.5.23-2) unstable; urgency=low

  * Remove lib/z3d which only builds on 32-bit architecture and isn't used
    by any libraries.

 -- Kan-Ru Chen <koster@debian.org>  Sun, 13 Mar 2011 02:09:41 +0800

picolisp (3.0.5.23-1) unstable; urgency=low

  * Initial release (Closes: #533011)

 -- Kan-Ru Chen <koster@debian.org>  Fri, 11 Mar 2011 07:44:02 +0800
